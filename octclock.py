#!/usr/bin/env python
import datetime
from decimal import *
def segs():
    tim=datetime.datetime.utcnow().time()
    secs=tim.hour*60*60+tim.minute*60+tim.second+Decimal(tim.microsecond)/1000000
    seg=int(secs*65536/86400)
    counters=[32768,4096,512,64,8,1]
    stock=[]
    for div in counters:
        stock+=[seg/div]
        seg-=seg/div*div
    return stock

def pretty():
    return ".".join([str(n) for n in segs()])

if __name__ == "__main__":
    print(pretty())

